﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Seguro> ListaSeguros = new List<Seguro>();

        public Seguro SeguroAsociado(int numeroPoliza)
        {
            Seguro seguroEncontrado = ListaSeguros.First(x => x.NumeroPoliza == numeroPoliza);
            return seguroEncontrado;
        }

        public bool RegistrarSiniestro(int dni, int numeroPoliza, DateTime fechaSiniestro)
        {
            Seguro seguroEncontrado = SeguroAsociado(numeroPoliza);
            if (seguroEncontrado!=null)
            {
                bool validarSeguroEncontrado = seguroEncontrado.ValidarSeguro(dni);
                return true;
            }
            return false;
        }
    }
}
