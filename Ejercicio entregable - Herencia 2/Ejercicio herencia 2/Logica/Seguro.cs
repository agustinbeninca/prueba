﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Seguro
    {
        public Persona Tomador { get; set; }
        public Persona Beneficiario { get; set; }
        public decimal PrimaAsegurada { get; set; }
        public int NumeroPoliza { get; set; }
        public decimal ValorMensual { get; set; }

        public decimal CalcularPorcentajeCobertura()
        {
            return ((ValorMensual*12)/PrimaAsegurada);
        }

        public abstract bool ValidarSeguro(int dni);


    }
}
