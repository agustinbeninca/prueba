﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    class Siniestro
    {
        public int Documento { get; set; }
        public DateTime FechaSiniestro { get; set; }
        public int NumeroPoliza { get; set; }

    }
}
