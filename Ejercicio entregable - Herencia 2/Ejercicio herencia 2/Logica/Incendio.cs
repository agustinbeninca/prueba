﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Incendio:Seguro
    {
        public decimal MetrosCuadrados { get; set; }
        public int CantidadMatafuegos { get; set; }
        public int CantidadEnchufes { get; set; }
        public bool EsVivienda { get; set; } //false=lugar de trabajo
        public decimal MontoBienesMateriales { get; set; }

        public override bool ValidarSeguro(int dni)
        {
            if (dni == Tomador.DNI)
            {
                if (CantidadMatafuegos>=CantidadEnchufes)
                {
                    if (EsVivienda==true)
                    {
                        if (MetrosCuadrados>400)
                        {
                            return false;
                        }
                        return true;
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
